export interface Tokens {
  accessToken: string;
  refreshToken: string;
}

export interface JwtToken {
  sub: string;
  iat: number;
  exp: number;
}

export type AsyncReturnType<T extends (...args: any) => any> = T extends (
  ...args: any
) => Promise<infer U>
  ? U
  : T extends (...args: any) => infer U
  ? U
  : any;
