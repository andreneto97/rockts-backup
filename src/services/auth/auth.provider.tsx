import { useEffect, useState } from "react";
import { createContainer } from "unstated-next";
import { getClaims, isAuthenticated, onResetTokens, onSetTokens } from ".";
import { AsyncReturnType } from "./types";

type FetchUser<T = any> = (id: string) => Promise<T>;
type FetchUserReturn<T> = AsyncReturnType<FetchUser<T>>;

type UseAuthUserReturn<T> = {
  authUser: T | undefined;
  refreshUser(): Promise<void>;
};

function useAuthUser<T>(fn: FetchUser<T>): UseAuthUserReturn<T> {
  const [authUser, setAuthUser] = useState<FetchUserReturn<T>>();

  const fetchAuthedUser = async () => {
    try {
      const claims = await getClaims();
      if ((await isAuthenticated()) && claims?.sub) {
        const user = await fn(claims?.sub);

        setAuthUser(user);
      } else {
        setAuthUser(undefined);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchAuthedUser();
    const resetTokens = onResetTokens(fetchAuthedUser);
    const setTokens = onSetTokens(fetchAuthedUser);
    return () => {
      resetTokens.unsubscribe();
      setTokens.unsubscribe();
    };
  }, []);
  return { authUser, refreshUser: fetchAuthedUser };
}

function useIsAuthenticated(initialState = false) {
  const [authenticated, setAuthenticated] = useState(initialState);

  const checkAuth = async () => {
    const isAuthed = await isAuthenticated();
    setAuthenticated(isAuthed);
  };

  useEffect(() => {
    checkAuth();
    const resetTokens = onResetTokens(checkAuth);
    const setTokens = onSetTokens(checkAuth);
    return () => {
      resetTokens.unsubscribe();
      setTokens.unsubscribe();
    };
  }, []);
  return authenticated;
}

type AuthProviderProps<T = any> = {
  fetchUser: FetchUser<T>;
};

type AuthHookOptions<T> = {
  isAuthenticated: boolean;
  authUser: T | undefined;
  refreshUser(): Promise<void>;
};

function useAuthHooks<T = any>(
  initialState?: AuthProviderProps<T>
): AuthHookOptions<T> {
  if (!initialState) {
    throw Error("Missing initial state to AuthProvider");
  }
  const { fetchUser } = initialState;
  const isAuthenticated = useIsAuthenticated();
  const { authUser, refreshUser } = useAuthUser<T>(fetchUser);
  return { isAuthenticated, authUser, refreshUser };
}

export function generateAuthContainer<T>() {
  return createContainer<AuthHookOptions<T>, AuthProviderProps<T>>(
    useAuthHooks
  );
}
