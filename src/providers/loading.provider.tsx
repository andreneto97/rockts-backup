import { useState } from "react";
import { createContainer } from "unstated-next";

const useHook = () => {
  const [isLoading, showLoading] = useState(false);
  return { isLoading, showLoading };
};

const Loading = createContainer(useHook);

export const LoadingProvider = Loading.Provider;
export const useLoading = Loading.useContainer;
