/* eslint-disable react-hooks/exhaustive-deps */
import { useState } from "react";
import { createContainer } from "unstated-next";

const useHook = () => {
  const [showSnackbar, setShowSnackbar] = useState(false);
  const [snackBarMessage, setSnackbarMessage] = useState<string | undefined>();
  const [isError, setIsError] = useState<boolean>(false);
  const clearSnackbar = () => {
    setSnackbarMessage(undefined);
    setShowSnackbar(false);
  };

  const notify = (message: string) => {
    setIsError(false);
    setSnackbarMessage(message);
    setShowSnackbar(true);
  };

  const notifyError = (err: any) => {
    setIsError(true);
    setShowSnackbar(true);
    const defaultMessage = "Something went wrong.";
    const axiosMsg = err.response?.data?.message;
    const errorMsg = err.message;
    setSnackbarMessage(axiosMsg || errorMsg || defaultMessage);
  };

  return { showSnackbar, snackBarMessage, notify, notifyError, clearSnackbar, isError };
};

const Snackbar = createContainer(useHook);

export const SnackbarProvider = Snackbar.Provider;
export const useSnackbar = Snackbar.useContainer;
