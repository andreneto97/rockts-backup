import { AsyncComponent } from "./components/AsyncComponent";
import { EmptyState } from "./components/EmptyState";
import { LoadingOverlay } from "./components/LoadingOverlay";
import { OutlinedTextField } from "./components/OutlinedTextField";
import { PasswordInput } from "./components/PasswordInput";
import { ProgressOverlay } from "./components/ProgressOverlay";
import { SnackbarOverlay } from "./components/SnackbarOverlay";
import { DumbComponent } from "./components/DumbComponent";

import { LayoutPrimitives } from "./helpers/layoutPrimitives";

import { useClassValidator } from "./hooks/useClassValidator";

import { useLoading, LoadingProvider } from "./providers/loading.provider";
import { useSnackbar, SnackbarProvider } from "./providers/snackbar.provider";
import { generateAuthContainer } from "./services/auth/auth.provider";

const Rockts = {
  // AsyncComponent,
  // EmptyState,
  DumbComponent,
  LoadingOverlay,
  // OutlinedTextField,
  PasswordInput,
  generateAuthContainer,
  // ProgressOverlay,
  SnackbarOverlay,
  LayoutPrimitives,
  useLoading,
  LoadingProvider,
  useSnackbar,
  SnackbarProvider,
  useClassValidator,
  hello: "Hello RockTS",
};

export { Rockts };

export default Rockts;
