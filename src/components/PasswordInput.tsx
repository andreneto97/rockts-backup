import { MaterialIcons } from "@expo/vector-icons";
import React, { useState } from "react";
import { DarkTheme, TextInput } from "react-native-paper";
import { TextInputProps } from "react-native-paper/lib/typescript/components/TextInput/TextInput";
import OutlinedTextField from "./OutlinedTextField";

interface Props extends Omit<TextInputProps, "error"> {
  error?: string | undefined;
}

export const PasswordInput: React.FC<Props> = (props) => {
  const [reveal, setReveal] = useState(false);

  return (
    <OutlinedTextField
      {...props}
      autoCompleteType="password"
      secureTextEntry={!reveal}
      right={
        <TextInput.Icon
          onPress={() => setReveal(!reveal)}
          name={() => (
            <MaterialIcons
              name={reveal ? "visibility-off" : "visibility"}
              size={20}
              // color={DarkTheme.colors.placeholder}
            />
          )}
        />
      }
    />
  );
};
