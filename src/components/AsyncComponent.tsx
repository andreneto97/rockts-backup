import isEmpty from "lodash/isEmpty";
import React, {
  Fragment,
  ReactElement,
  useEffect,
  useRef,
  useState,
} from "react";
import { Animated, Platform, StyleSheet } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { EmptyState, EmptyStateProps } from "./EmptyState";

type Props = {
  loading: boolean;
  data: any;
  error: any;
  emptyProps: EmptyStateProps;
  EmptyComponent?: ReactElement;
  LoadingComponent?: ReactElement;
  ErrorComponent?: ReactElement;
  DataComponent: ReactElement;
  indicatorColor?: string;
};

const isWeb = Platform.OS === "web";

const duration = 1000;

export const AsyncComponent: React.FC<Props> = ({
  loading,
  data,
  error,
  emptyProps,
  EmptyComponent,
  DataComponent,
  ErrorComponent,
  LoadingComponent,
  indicatorColor = "black",
}) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const [isPending, setIsPending] = useState(false);

  useEffect(() => {
    if (!loading) {
      fadeOut();
      setTimeout(() => setIsPending(false), duration);
    } else {
      setIsPending(true);
      fadeIn();
    }
  }, [loading]);

  const fadeIn = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: duration / 2,
      useNativeDriver: isWeb ? false : true,
    }).start();
  };

  const fadeOut = () => {
    // Will change fadeAnim value to 0 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: duration,
      useNativeDriver: isWeb ? false : true,
    }).start();
  };

  const LoadingIndicator = () => {
    if (!isPending) return null;
    return (
      LoadingComponent ?? (
        <Animated.View style={[styles.loadingWrapper, { opacity: fadeAnim }]}>
          <ActivityIndicator size={70} color={indicatorColor} />
        </Animated.View>
      )
    );
  };

  if (error) {
    return ErrorComponent ?? <EmptyState message="error" />;
  }

  if (isEmpty(data) && !isPending) {
    return EmptyComponent ?? <EmptyState {...emptyProps} />;
  }
  return (
    <Fragment>
      <LoadingIndicator />
      {DataComponent}
    </Fragment>
  );
};

const styles = StyleSheet.create({
  loadingWrapper: {
    flexGrow: 1,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,0.5)",
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    zIndex: 99,
    justifyContent: "center",
  },
});
