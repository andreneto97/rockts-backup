import React from "react";
import { View, Text } from "react-native";

export const DumbComponent: React.FC = () => (
  <View>
    <Text>Hello, I'm a component</Text>
  </View>
);
