import { MaterialCommunityIcons } from "@expo/vector-icons";
import React from "react";
import { Paragraph, Title } from "react-native-paper";
import { Column, Container, Spacer } from "../helpers/layoutPrimitives";

export type EmptyStateProps = {
  title?: string;
  message?: string;
  iconName?: string;
  fontFamily?: string;
  titleColor?: string;
  iconColor?: string;
  backgroundColor?: string;
};

export const EmptyState: React.FC<EmptyStateProps> = ({
  title = "Nothing to see here yet.",
  message = "Please check later.",
  iconName = "package",
  titleColor = "black",
  iconColor = "black",
  backgroundColor = "white",
  fontFamily,
}) => (
  <Column
    flex={1}
    justifyContent="center"
    alignItems="center"
    backgroundColor={backgroundColor}
  >
    <Container center flex={1}>
      {/* @ts-ignore */}
      <MaterialCommunityIcons name={iconName} size={80} color={iconColor} />
      <Spacer />
      <Title style={[{ color: titleColor, fontFamily }]}>{title}</Title>
      <Paragraph>{message}</Paragraph>
    </Container>
  </Column>
);
