import React, { useEffect, useRef, useState } from "react";
import { Animated, Dimensions, Platform, StyleSheet } from "react-native";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import { Colors, Portal, Text } from "react-native-paper";

const isWeb = Platform.OS === "web";

const duration = 300;
type ProgressOverlayProps = {
  progress: number;
};
export const ProgressOverlay: React.FC<ProgressOverlayProps> = ({
  progress,
}) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;

  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    if (progress === 1) {
      fadeOut();
      setTimeout(() => setIsVisible(false), duration);
    } else if (isVisible === false && progress > 0) {
      setIsVisible(true);
      fadeIn();
    }
  }, [progress]);

  const fadeIn = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: duration / 2,
      useNativeDriver: isWeb ? false : true,
    }).start();
  };

  const fadeOut = () => {
    // Will change fadeAnim value to 0 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: duration,
      useNativeDriver: isWeb ? false : true,
    }).start();
  };
  if (!isVisible) {
    return null;
  }
  return (
    <Portal>
      <Animated.View style={[styles.wrapper, { opacity: fadeAnim }]}>
        <AnimatedCircularProgress
          fill={progress * 100}
          size={100}
          width={10}
          tintColor={Colors.white}
        >
          {(fill) => (
            <Text
              style={{ color: Colors.white, fontSize: 18 }}
            >{`${fill.toFixed(0)}%`}</Text>
          )}
        </AnimatedCircularProgress>
      </Animated.View>
    </Portal>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "rgba(0,0,0,0.5)",
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    zIndex: 9999,
    left: 0,
    top: 0,
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width,
    padding: 20,
  },
});
