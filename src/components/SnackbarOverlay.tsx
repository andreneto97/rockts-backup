import React from "react";
import { Portal, Snackbar, useTheme } from "react-native-paper";
import { useSnackbar } from "../providers/snackbar.provider";

export const SnackbarOverlay: React.FC<{}> = () => {
  const { showSnackbar, snackBarMessage, isError, clearSnackbar } = useSnackbar();
  const { colors } = useTheme();

  return (
    <Portal>
      <Snackbar
        visible={showSnackbar}
        duration={3000}
        onDismiss={() => clearSnackbar()}
        style={isError ? { backgroundColor:  colors.error }: {}}
      >
        {snackBarMessage}
      </Snackbar>
    </Portal>
  );
};
