import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { Caption, TextInput } from "react-native-paper";
import { TextInputProps } from "react-native-paper/lib/typescript/components/TextInput/TextInput";
import { Column } from "../helpers/layoutPrimitives";

interface Props extends Omit<TextInputProps, "error"> {
  error?: string | undefined;
}

const OutlinedTextField: React.FC<Props> = (props) => {
  const inputProps = { ...props };
  const { error } = inputProps;
  const [isFocused, setIsFocused] = useState(false);
  const hasError = !!error;

  return (
    <Column flex={1}>
      <TextInput
        {...inputProps}
        style={[styles.input]}
        theme={inputProps.theme}
        placeholderTextColor={"#002467"}
        underlineColor={"#002467"}
        error={hasError}
        onFocus={() => {
          setIsFocused(true);
        }}
        onBlur={() => {
          setIsFocused(false);
        }}
      />

      {hasError && <Caption style={styles.errorMessage}>{error}</Caption>}
    </Column>
  );
};

const styles = StyleSheet.create({
  input: {
    backgroundColor: "transparent",
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
  },
  errorMessage: {
    color: "#E30613",
    paddingLeft: 10,
  },
});

export default OutlinedTextField;
