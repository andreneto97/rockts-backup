import React from "react";
import { StyleProp, View, ViewStyle, StyleSheet } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

interface GenericProps extends ViewStyle {
  center?: boolean;
  scroll?: boolean;
}

export const Container: React.FC<GenericProps> = (props) => {
  const { children, center, scroll, flexGrow, flex, ...rest } = props;

  const baseStyle: StyleProp<ViewStyle> = StyleSheet.flatten([
    center && {
      justifyContent: "center",
      alignItems: "center",
    },
    //Scroll view dont work with flex:1. hack to keep compatibility
    //only apply hack if scroll is enabled
    {
      backgroundColor: "transparent",
      flexGrow: !scroll ? flexGrow : flexGrow ?? flex,
      flex: !scroll ? flex : 0,
    },

    { ...rest },
  ]);

  if (scroll) {
    return (
      <ScrollView contentContainerStyle={baseStyle}>{children}</ScrollView>
    );
  }

  return <View style={baseStyle}>{children}</View>;
};

export const Column: React.FC<GenericProps> = (props) => {
  const { children, ...rest } = props;
  return (
    <Container flexDirection="column" {...rest}>
      {children}
    </Container>
  );
};

export const Row: React.FC<GenericProps> = (props) => {
  const { children, ...rest } = props;
  return (
    <Container flexDirection="row" {...rest}>
      {children}
    </Container>
  );
};

type SpacerProps = {
  size?: number;
};

export const Spacer: React.FC<SpacerProps> = ({ size = 20 }) => {
  return <View style={{ height: size, width: size }} />;
};

export const LayoutPrimitives = {
  Column,
  Spacer,
  Row,
  Container,
};
