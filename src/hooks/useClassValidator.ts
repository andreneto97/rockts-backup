import { validate as classValidate, ValidationError } from "class-validator";
import { useCallback, useReducer, useState } from "react";

interface Class<T> {
  new (): T;
}

// type ObjectWithKeysOf<T> = { [P in keyof T]: T[P] };

type StringObjectWithKeysOf<T> = { [P in keyof T]: string };

type KeysOf<T> = Extract<keyof T, string>;

type FieldsOf<T> = { [P in keyof T]: T[P] };

type MessagesOf<T> = StringObjectWithKeysOf<T> | undefined;

type UpdateFieldFunction<T> = (key: KeysOf<T>, value: any) => void;

interface ValidateResult<T> {
  data: T | null;
  errors: MessagesOf<T>;
}

type UpdateOneAction<T> = { type: KeysOf<T>; payload: any };
type UpdateAllAction<T> = { type: "UPDATE_ALL"; payload: any };

type State<T> = FieldsOf<T>;

const cleanFromValidator = <T>(dto: T, errors: ValidationError[]) => {
  const dtoKeys = Object.keys(dto) as KeysOf<T>[];

  const msgs = {} as StringObjectWithKeysOf<T>;
  dtoKeys.forEach((key) => {
    const fieldError = errors.find((error) => {
      return error.property === key;
    });
    if (!fieldError) {
      return;
    }
    // Get Constraints
    const { constraints } = fieldError;
    // Get first validation message
    if (!constraints) return;
    msgs[key] = constraints[Object.keys(constraints)[0]];
  });
  return msgs;
};

export const useClassValidator = <T extends object>(DTO: Class<T>) => {
  const [dto, setDto] = useState<T>(new DTO());
  const [msgs, setMsgs] = useState<StringObjectWithKeysOf<T>>();
  const [state, dispatch] = useReducer(reducer, { ...dto });

  function reducer(
    state: State<T>,
    action: UpdateOneAction<T> | UpdateAllAction<T>
  ): State<T> {
    switch (action.type) {
      case "UPDATE_ALL":
        return { ...state, ...action.payload };

      default:
        return { [action.type]: action.payload } as State<T>;
    }
  }

  const onChange: UpdateFieldFunction<T> = (key, value) => {
    // if fields is empty create it
    dto[key] = value;
    // Need to spread to update state d
    setDto(dto);
    dispatch({ type: key, payload: value });
  };

  const setFields = (values: T) => {
    dispatch({ type: "UPDATE_ALL", payload: values });
    setDto(Object.assign(dto, values));
  };

  const validate = useCallback(async (): Promise<ValidateResult<T>> => {
    const results = await classValidate(dto);

    const isValid = results.length === 0;

    // If valid clear messages and return
    if (isValid) {
      setMsgs(undefined);
      return { data: dto, errors: {} as StringObjectWithKeysOf<T> };
    }

    const validationMsgs = cleanFromValidator<T>(dto, results);
    setMsgs(validationMsgs);
    return { data: null, errors: validationMsgs };
  }, [dto]);

  return { fields: state, onChange, msgs, validate, setFields };
};
